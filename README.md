# NDL Formal Semantics 

This repository contains a formal semantics for the Neighborgood Definition Language.
It's using an underlying formalism knows as [k-framework](https://kframework.org).

## How To Use:

Given an already installed k-framework:

```bash
# kompile --backend haskell ndl.k 
# krun examples/kempe.ndl % kempe chain neigbhorhood for a 5-node graph
```

## Roadmap:

- [x] find a clear enough syntax
- [x] embed typed constraint network in the language
- [x] define basic operators
- [x] define "forall" (universal quantifier)
- [x] define (functional) "map" over variables based on their indexes
- [x] define "Least Fix Point" operator.
- [x] create examples of real neighborhoods
  - [x] 2-opt
  - [x] kempe-chain
  - [ ] column swap
- [ ] test
